package com.example.ian.oopandencapsulation;



public class BankAccount {
    private static final String TAG = "BankAccount";

    // TODO: 23/06/2017 Add instancce variables for a bank account at a minimum the variables accountName and balance should be present
    // TODO: 23/06/2017 Encapsulate the instance variables of the class

    // TODO: 23/06/2017 Add method to deposit funds. Do not allow negative amounts to be deposited. Display balance in LogCat after successful deposit

    // TODO: 23/06/2017 Add method to withdraw funds. Do not allow withdrawals greater than current balance. Display balance in LogCat after successful withdrawal

}
